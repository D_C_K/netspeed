# coding: utf-8
import speedtest
from time import sleep
import datetime
import csv
import matplotlib.pyplot as plt
import os
class Netspeed:
    def __init__(self):
        '''Measure the Internet download and upload speed in Mbits/s'''
        self.test = speedtest.Speedtest()
        print('Finding best Server')
        self.best = self.test.get_best_server()
        self.host = self.best['host']
        print(f'Best Server: {self.host}')
    
    def get_downspeed(self):
        print('Testing download speed')
        return round(self.test.download() / (1024 * 1024), 2)
    
    def get_upspeed(self):
        print('Testing upload speed')
        return round(self.test.upload() / (1024 * 1024), 2)
    
    def save_to_csv(self, name, directory='data/'):
        '''saves the internet speed in a given file in csv format'''
        now = datetime.datetime.now()
        time = now.strftime('%H:%M')
        date = now.strftime('%d-%m-%Y')
        path = directory + str(name)
        
        #checks if the path exists and creates it if not
        if os.path.exists(directory):
            pass
        else:
            os.makedirs(directory)
            print('created directory ' + directory)
            
        #ckecks if the file exists and creates it if not. Also writes the first row
        if not os.path.isfile(directory + str(name)):
            with open(directory + str(name), 'w') as f:
                csv_writer = csv.writer(f)
                csv_writer.writerow(['date', 'time', 'host', 'upspeed', 'downspeed'])
                print('created file ' + str(directory) + str(name))
        
        #appends the measured data to the csv file
        with open(directory + str(name), 'a') as f:
            csv_writer = csv.writer(f)
            csv_writer.writerow([date, time, self.host, self.get_upspeed(), self.get_downspeed()])
            print('saved data in ' + str(directory) + str(name))
       
        
        
