# coding: utf-8
import datetime
import os
import internet_speed
from time import sleep
import internet_speed 
def save_log_daily(seconds_interval=10):
    while True:
        filename = str(datetime.datetime.now().strftime('%Y%m%d')) + '.csv'
        print(filename)
        try:
            internet_speed.Netspeed().save_to_csv(name=filename, directory='/var/www/html/netspeed_data/')
        except:
            print('error while Netspeed-test')
        sleep(seconds_interval)
        
save_log_daily(20)
